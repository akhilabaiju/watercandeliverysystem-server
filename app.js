require('dotenv').config(); // Load dotenv
const express = require("express");
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const helmet = require('helmet');


app.use(cors());
app.use(bodyParser.json());
app.use(express.json());

const database = require("./src/utils/database");
const routes = require("./src/routes");

app.use(express.static('./src/public'));
database();

app.use("/api", routes);
app.use(helmet);
app.use('/api', cors({
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'Authorization'],
    credentials: true
}));

app.options("*", cors());

// Error handling middleware
app.use((req, res, next) => {
    res.status(404).send('Not Found');
});

const port = process.env.PORT || 3001; // Use PORT from environment variables or default to 3001
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
