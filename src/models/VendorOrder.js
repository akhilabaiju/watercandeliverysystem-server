const mongoose = require('mongoose');

const VendororderSchema = new mongoose.Schema({
  
  manufactureId :{ type: String, required: true},
  userId :{ type: String, required: true},
  vendorId :{ type: String, required: true},
  productId :{ type: String, required: true},
  quantity: { type: Number, required: true },
  amount:{ type: Number, required: true },
  totalAmount:{ type: Number, required: true },
  createdAt: { type: Date, default: Date.now }
});

const VendorOrderItem = mongoose.model('VendorOrderItem', VendororderSchema);

module.exports = VendorOrderItem;
