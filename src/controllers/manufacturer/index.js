

const UsersData = require("../../models/Userdata");


 const getAllManufacturers = async (req, res) => {

 
      try {
        const manufacturer=await UsersData.find({"userType":"manufacturer"})
        if (!manufacturer) { 
    return res.status(404).json({ message: 'No usernames found' });
  }else{
     res.json(manufacturer);
  }
} catch (error) {
  console.error('Error fetching products data:', error);
  res.status(500).json({ message: 'Internal server error' });
}
}



const getManufacturerById = async (id, res) => {
    try {
        const user = await UsersData.findById(id);
         if (!user) {
     return res.status(404).json({ message: 'User not found' });
   }else{
  
   res.json(user.firstName +" "+user.lastName);
   }
 } catch (error) {
   console.error('Error fetching user data:', error);
   res.status(500).json({ message: 'Internal server error' });
 }
}

 
module.exports = {getAllManufacturers, getManufacturerById };



