const express = require('express');
const router = express.Router();

const productsController =require('../../controllers/products')

// Import the multer middleware for file uploads
const path= require("path");
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
     cb(null, 'src/public/images/uploads');
     
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
      }
    });
 const upload = multer({ storage: storage });

router.post('/add', upload.single('image'), productsController.addproducts);

 router.get('/byproductid/:id', (req, res) => {
    const productId = req.params.id;
    productsController.getProductsById(productId, res);
});


 router.get('/getAllProducts',productsController.getAllProducts)

 router.get('/byuserid/:id', (req, res) => {
    const userId = req.params.id;
    productsController.getProductsByUserId(userId, res);
});



router.get('/pnumberbyid/:id', (req, res) => {
    const productId = req.params.id;
    productsController.getpNumberById(productId, res);
});

router.put('/update',  productsController.updateProduct);

 module.exports = router
