const express = require('express');
const router = express.Router();

const userController =require('../../controllers/users')

router.get('/byid/:id', (req, res) => {
        const userId = req.params.id;
        if(userId)
        userController.getUserById(userId, res);
    });
    router.get('/getAllUsers',userController.getAllUsers)
    router.get('/getVendors',userController.getVendors)
    router.put('/update',userController.updateUser)
    router.post('/addresses',userController.addAddress);
    //router.get('/addresses/:id',userController.getAddressByUserId);
    router.get('/addresses/:id', (req, res) => { 
        const userId = req.params.id;
        if(userId)
        userController.getAddressByUserId(userId, res);
    });
           //  addresses/addressbyid/${id}
    router.get('/addresses/addressbyid/:id',userController.getAddressById)
    router.put('/removeaddress/:id',userController.deleteAddress);

    module.exports = router
