
const express = require('express');
const router = express.Router();
const accounts = require('./accounts');
const products = require('./products');
const users = require('./users');
const mail = require('./mail');
const manufacturer = require ('./manufacturer');
const cart = require('./cart');
const orders = require('./orders');
const vendor = require('./vendor');

router.use('/accounts', accounts);
router.use('/products', products);
router.use('/user', users);
router.use('/mail', mail);
router.use('/manufacturer', manufacturer);
router.use('/cart', cart);
router.use('/orders', orders);
router.use('/vendor', vendor);

module.exports = router; 
