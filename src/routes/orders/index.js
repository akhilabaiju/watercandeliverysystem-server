const express = require('express');
const router = express.Router();

    const orderController =require('../../controllers/orders')

    router.post('/placeorder', orderController.placeOrder)
    router.post('/placeorderv', orderController.placeOrderv)
    router.get('/orders/:id',orderController.getOrdersById)

    router.get('/getManufactureorders/:id',orderController.getManufactureOrders)

     module.exports = router
